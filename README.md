# Web Spectrum Viewer

This is a proof of concept project to demonstrate how to use Python Dash to provide a simple web interface for viewing files containing RF spectrum.

## Installation

Install dependencies with:

```shell
pip install numpy dash dash_bootstrap_components pandas
```

## Usage

This project includes a simple set of simulated data at start up.  Other data can be uploaded to the page for display.  This data must be in the form of a Python pickle .p file, which contains a list of dictionaries.  Each dictionary must contain the following keys:
* Name: The name of the trace
* Frequency_Hz:  The frequency, in units of Hz, of the PSD data.  The length of this must equal the length of PSD_dB.
* PSD_dB: The PSD vector, in arbitrary units.  The length of this must equal the length of Frequency_Hz

