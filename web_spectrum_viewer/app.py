# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.
import pickle
import base64

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import plotly.express as px
import plotly.graph_objs as go
import pandas as pd
import dash_table

from dash.dependencies import Input, Output

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.DARKLY])
plotly_template = "plotly_dark"

def rrc_response(T, beta, f_hz, cf_hz):
    # from https://en.wikipedia.org/wiki/Raised-cosine_filter
    h = np.zeros(len(f_hz))
    for idx, f_hz_ in enumerate(f_hz):
        f_hz_ = f_hz_ - cf_hz
        a = (1 - beta) / 2 / T
        if np.abs(f_hz_) <= a:
            h[idx] = 1
        elif np.abs(f_hz_) > a and np.abs(f_hz_) <= (1 + beta) / (2 * T):
            h[idx] = np.sqrt(0.5 * (1 + np.cos((np.pi*T/beta) * (np.abs(f_hz_) - a))))
        else:
            h[idx] = 0.0
    return h

#
# Generate some simulated data
#

def generate_simulated_traces():
    f_hz = np.linspace(-50, 50, 10001)
    traces = []
    p_db = 10 * rrc_response(1 / 5, 0.35, f_hz, 10) + 5 * rrc_response(1 / 15, 0.35, f_hz, -20)
    p_db = p_db + np.random.randn(len(f_hz)) / 5
    trace_dict = {
        'Name': "Satellite 1 - Horizontal",
        "Satellite": "Satellite 1",
        "Polarization": "Horizontal",
        'Frequency_Hz':  f_hz,
        'PSD_dB': p_db
    }
    traces.append(trace_dict)

    p_db = 10 * rrc_response(1 / 1, 0.35, f_hz, -15) + 5 * rrc_response(1 / 1, 0.35, f_hz, -10) + 5 * rrc_response(1 / 1, 0.35, f_hz, -5)
    p_db = p_db + np.random.randn(len(f_hz)) / 5
    trace_dict = {
        # 'trace': pd.DataFrame({"PSD": p_db, "Frequency": f_hz}),
        'Name': "Satellite 1 - Vertical",
        "Satellite": "Satellite 1",
        "Polarization": "Vertical",
        'Frequency_Hz':  f_hz,
        'PSD_dB': p_db
    }
    traces.append(trace_dict)

    p_db = 10 * rrc_response(1 / 2, 0.35, f_hz, -25) + 5 * rrc_response(1 / 3, 0.35, f_hz, 0) + 5 * rrc_response(1 / 4, 0.35, f_hz, 25)
    p_db = p_db + np.random.randn(len(f_hz)) / 5
    trace_dict = {
        # 'trace': pd.DataFrame({"PSD": p_db, "Frequency": f_hz}),
        'Name': "Satellite 2 - Vertical",
        "Satellite": "Satellite 2",
        "Polarization": "Vertical",
        'Frequency_Hz':  f_hz,
        'PSD_dB': p_db
    }
    traces.append(trace_dict)

    return traces


traces = generate_simulated_traces()
# pickle.dump(traces, open( "traces.p", "wb" ) )


table_cols = list(traces[0])
table_cols.remove('Frequency_Hz')
table_cols.remove('PSD_dB')
df_table = pd.DataFrame({col: [td[col] for td in traces] for col in table_cols})


def generate_table(df, max_rows=10):
    return \
        dash_table.DataTable(
            id='spectrum_table',
            columns=[{"name": i, "id": i} for i in df.columns],
            data=df.to_dict('records'),
            sort_action='native',
            filter_action='native',
            row_selectable='multi',
            style_header={'backgroundColor': 'rgb(30, 30, 30)'},
            style_cell={
                'backgroundColor': 'rgb(50, 50, 50)',
                'color': 'white'
            },
        )


@app.callback(
    Output(component_id='example-graph', component_property='figure'),
    Input(component_id='spectrum_table', component_property='selected_rows')
)
def update_spectrum(selected_rows):
    fig = go.Figure()
    #fig.update_layout(template=plotly_template, selectdirection='h', dragmode='select')
    fig.update_layout(template=plotly_template)
    if selected_rows is not None and len(selected_rows) > 0:
        for selected_row in selected_rows:
            fig.add_scatter(x=traces[selected_row]['Frequency_Hz'], y=traces[selected_row]['PSD_dB'], name=traces[selected_row]['Name'])
    return fig

@app.callback(
    [Output(component_id='spectrum_table', component_property='data'),Output(component_id='spectrum_table', component_property='columns')],
    [Input("upload-data", "filename"), Input("upload-data", "contents")],
    prevent_initial_call=True
)
def update_output(uploaded_filename, uploaded_content):
    # Save uploaded files and regenerate the file list.

    if uploaded_content is not None:
        content_type, content_string = uploaded_content.split(',')
        decoded = base64.b64decode(content_string)
        traces = pickle.loads(decoded)

        table_cols = list(traces[0])
        table_cols.remove('Frequency_Hz')
        table_cols.remove('PSD_dB')
        columns = [{"name": i, "id": i} for i in table_cols]

    return traces, columns

    return [], [{'name': 'Name', 'id': 'Name'}, {'name': 'Satellite', 'id': 'Satellite'}, {'name': 'Polarization', 'id': 'Polarization'}]


fig = go.Figure()

app.layout = html.Div(children=[
    html.H1(children='PSD Viewer'),

    dcc.Upload(
            id="upload-data",
            children=html.Div(
                ["Drag and drop or click to select a file to upload."]
            ),
            style={
                "width": "97%",
                "height": "60px",
                "lineHeight": "60px",
                "borderWidth": "1px",
                "borderStyle": "dashed",
                "borderRadius": "5px",
                "textAlign": "center",
                "margin": "10px",
            },
            multiple=False,
        ),

    dcc.Graph(
        id='example-graph',
        figure=fig
    ),

    html.Div(children=[
        html.H4(children='Traces'),
        generate_table(df_table)]
    )
])


if __name__ == '__main__':
    app.run_server(debug=False)